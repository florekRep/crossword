package crossword.browser;

import crossword.logic.*;
import java.io.*;

/**
 * Interfej odpowiedzialny za zapisywanie krzyżówek
 * @author Florek
 *
 */
public interface Writer {
	void write(Crossword cw) throws IOException, BrowserException;
}
