package crossword.browser;

import java.util.*;
import java.io.*;
import crossword.logic.*;

/**
 * Interfejs odpoweidzialny za wczytywanie krzyżówek z katalogu
 * @author Florek
 *
 */
public interface Reader {
	ArrayList<Crossword> getAllCws() throws IOException, BrowserException;
}
