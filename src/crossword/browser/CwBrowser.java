package crossword.browser;

import java.io.*;
import java.util.*;
import java.util.regex.Pattern;

import crossword.dictionary.InteliCwDB;
import crossword.logic.Crossword;
import crossword.strategy.Strategy;

/**
 * Klasa odpowiedzialna za przeglądanie, zapisywanie, wczytywanie krzyżówek
 * Zarządza krzyżówkami
 * @author Florek
 *
 */
public class CwBrowser {
	/** lista wczytanych krzyżówek*/
	private ArrayList<Crossword> listOfCw=new ArrayList<Crossword>();
	/** krzyżówka aktualnie wyświetlana */
	private Crossword present;
	/** aktualnie wybrany katalog, do niego będą zapisywane, lub z niego są odczytywane krzyżówki */
	private File directory;
	/** implementacja interfejsu Reader odpowiedzialnego za czytanie krzyżówek */
	private Reader reader= new Reader() {
		@Override
		
		/**
		 * Wczytuje wszystkie krzyżówki z aktualnie wybranego katalogu do listy
		 */
		public ArrayList<Crossword> getAllCws() throws IOException, BrowserException {
			if(directory==null) if(directory==null) throw new BrowserException("Katalog nie został wybrany");
			 ArrayList<Crossword> loaded=new ArrayList<Crossword>();
			 File[] content = directory.listFiles(new FilenameFilter() {		//wybieramy tylko pliki posiadające liczby w nazwie
				private Pattern pattern=Pattern.compile("[0-9]+");
				@Override
				public boolean accept(File file, String path) {
					return pattern.matcher(path).matches();
				}
			 });
			 for(File file: content) {		//po koleji dla każdego pliku z krzyżówką
				 FileInputStream fis=new FileInputStream(file);		//otwarcie strumieni
				 try {
					 ObjectInputStream ois=new ObjectInputStream(fis);
					 try {
						Crossword cw=new Crossword(Long.parseLong(file.getName())); //tworzenie nowej krzyżówki z nadaniem jej numeru ID
					 	cw.deserializeCw(ois);		//uruchamianie deserializacji
					 	loaded.add(cw);	//dodanie do listy			
					 } finally {
					 	ois.close();		//po zakończeniu zamykam strumień
					 }
				 } catch(StreamCorruptedException e) {
					 //oznacza że plik nie jest krzyżówą więc ignorujemy
				 } catch(ClassNotFoundException e) {
					 throw new RuntimeException("Coś poszło BARDZO źle");	//nie powinien nigdy polecieć
				 }
			 }
			 if(loaded.size()==0)
				 throw new BrowserException("W katalogu nie ma krzyżówek");
			 return loaded;
		}
	};
	/** implementacja interfejsu Writer służy do zapisywania krzyżówek */
	private Writer writer= new Writer() {
		/**
		 * zapisuje aktualnie wybraną krzyżówkę do wybranego katalogu
		 */
		@Override
		public void write(Crossword cw) throws IOException, BrowserException {
			if(directory==null) throw new BrowserException("Katalog nie został wybrany");
			if(present==null) throw new BrowserException("Brak krzyżówki do zapisu");
			File newCw=new File(directory, getUniqueID()+"");	//tworzenie pliku do zapisu
			FileOutputStream fos=new FileOutputStream(newCw);	//tworzenie strumieni
			ObjectOutputStream oos=new ObjectOutputStream(fos);
			try {
				cw.serializeCw(oos); 	//zapis
			} finally {
				oos.close();
			}
		}
		/** zwraca aktualny czas w milisekundach*/
		private long getUniqueID() {
			return new Date().getTime(); 
		}
	};
	/**
	 * Ustawia nowy katalog do zapisu/odczytu
	 * @param dirName ścieżka katalogu
	 * @throws IOException błędna ścieżka
	 */
	public void setDir(String dirName) throws IOException {
		File oldDir=directory;
		directory=new File(dirName);
		if(!directory.exists() || !directory.isDirectory()) {
			String msg=directory+" nie jest właściwym katalogiem";
			directory=oldDir;
			throw new IOException(msg);
		}
	}
	
	/**
	 * Zwraca aktualnie wybrany katalog
	 * @return aktualnie wybrany katalog
	 */
	public File dirPath() {
		return directory; 
	}
	
	/**
	 * Zwraca krzyżówki wczytane podczas ostatniego wywołania metody load;
	 * @return wczytane krzyżówki
	 */
	public ArrayList<Crossword> getAvailableCw() {
		return listOfCw;
	}
	/**
	 * Zmienia aktualnie wybraną krzyżówkę
	 * @param cw krzyżówka
	 */
	public void setPresentCw(Crossword cw) {
		present=cw;
	}
	/**
	 * Zwraca aktualnie wybraną krzyżówkę
	 * @return krzyżówka
	 */
	public Crossword getPresentCw() {
		return present;
	}
	/**
	 * Zapisuje aktualnie wybraną krzyżówkę
	 * @throws IOException problem z zapisem
	 * @throws BrowserException nieprawidłowe dane
	 */
	public void save() throws IOException, BrowserException {
		writer.write(present);
	}
	/**
	 * Odczytuje krzyżówki z aktualnie wybranego katalogu
	 * @throws IOException problem z odczytem
	 * @throws BrowserException nieprawidłowe dane
	 */
	public void load() throws IOException, BrowserException {
		listOfCw.clear();
		listOfCw=reader.getAllCws();
	}
	/**
	 * Generuje krzyżówkę. Deleguje wywołanie do klasy Crossword
	 * @param s	strategia
	 * @param height wysokość
	 * @param width szerokość
	 * @param initDB baza danych
	 */
	public void generate(Strategy s, int height, int width, InteliCwDB initDB) {
		present=new Crossword(height, width, initDB);
		present.generate(s);
	}
}
