package crossword.browser;

public class BrowserException extends Exception {
	public BrowserException(String msg) { super(msg); }
}
