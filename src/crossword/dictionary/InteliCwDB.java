package crossword.dictionary;

import java.io.*;
import java.util.*;
import java.util.regex.*;

/**
 * Ulepszona wersja bazy danych rozszerzająca początkowy interfejs
 * @author Florek
 *
 */
public class InteliCwDB extends CwDB {
	private Random rand=new Random(10);
	
	/**
	 * Tworzenie bazy danych na podstawie pliku z hasłami
	 * @see crossword.dictionary.CwDB
	 * @param fname nazwa pliku z danymi
	 * @throws IOException błędy związane z plikiem
	 */
	public InteliCwDB(String fname) throws IOException {
		super(fname);
	}
	
	/**
	 * Wyszukuje w bazie wszystkie wpisy paszujące do zadanego wzorca
	 * @param pattern regex do wyszukania
	 * @return lista haseł pasujących do regexpa
	 */
	public LinkedList<Entry> findAll(String pattern) {
		LinkedList<Entry> res=new LinkedList<Entry>();
		Pattern regex=Pattern.compile(pattern, Pattern.CASE_INSENSITIVE); //użycie flagi CASE_INSENSITIVE
		for(Entry e: dict)
			if(regex.matcher(e.getWord()).find())
				res.add(e);
		return res;	
	}
	
	/**
	 * Zwraca losowy wpis z bazy
	 * @return losowy wpis z bazy, lub null jeśli baza jest pusta
	 */
	public Entry getRandom() {
		if(dict.size()==0)
			return null;
		return dict.get(rand.nextInt(dict.size()));
	}
	
	/**
	 * Zwraca losowy wpis pasujący do wzorca
	 * @param pattern regex do wyszukania
	 * @return losowy wpis pasujący do regexpa, lub null jeśli nic nie znalazł
	 */
	public Entry getRandom(String pattern) {
		LinkedList<Entry> matches=findAll(pattern);
		if(matches.isEmpty())
			return null;
		return matches.get(rand.nextInt(matches.size()));
	}
	
	/**
	 * Zwraca losowy wpis o zadanej długości
	 * @param length długość wpisu
	 * @return losowy wpis o podanej długości, lub null jeśli nic nie znalazł
	 */
	public Entry getRandom(int length) {
		LinkedList<Entry> matches=new LinkedList<Entry>();
		for(Entry e: dict)
			if(e.getWord().length()==length)
				matches.add(e);
		if(matches.size()==0)
			return null;
		return matches.get(rand.nextInt(matches.size()));
	}
	
	/**
	 * Dodaje wpis do bazy danych, z zachowaniem kolejności alfabetycznej (case insensitive)
	 * @see crossword.dictionary.CwDB#add(String, String)
	 */
	public void add(String word, String clue) {
		ListIterator<Entry> iter;
		Entry entry=new Entry(word,clue);
		if(dict.indexOf(entry)!=-1) //sprawdzanie czy wpis już istnieje w bazie
			return;
		for(iter=dict.listIterator(); iter.hasNext(); ) //list iterator może iść zarówno w przód jak i w tył
			if(word.compareToIgnoreCase(iter.next().getWord())<0) {
				iter.previous();
				break;
			}
		iter.add(entry);
	}
}
