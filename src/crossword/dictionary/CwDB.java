package crossword.dictionary;

import java.io.*;
import java.util.*;

/**
 * Baza danych do generowania krzyżówek
 * @author Florek
 *
 */
public class CwDB {
	protected LinkedList<Entry> dict=new LinkedList<Entry>();
	
	/**
	 * Tworzy bazę danych na podstawie pliku tekstowego
	 * @param fname nazwa pliku z danymi
	 * @throws IOException błędy związane z plikiem
	 */
	public CwDB(String fname) throws IOException{
		createDB(fname);
	}
	
	/**
	 * Dodawanie hasła do bazy danych
	 * @param word hasłow
	 * @param clue wskazówka
	 */
	public void add(String word, String clue) {
		Entry fresh=new Entry(word,clue);
		if(dict.indexOf(fresh)==-1)  //sprawdzenie czy się nie powtarzają
			dict.add(fresh);
	}
	
	/**
	 * Pobranie wpisu związanego z hasłem
	 * @param word hasło
	 * @return wpis związany z podanym hasłem
	 */
	public Entry get(String word) {
		int index=dict.indexOf(new Entry(word,null)); //tymczasowy obiekt posłuży do wyszukania
		if(index==-1)
			return null;
		else
			return dict.get(index);
	}
	
	/**
	 * Usunięcie wpisu powiązanego z hasłem
	 * @param word hasło
	 */
	public void remove(String word) {
		dict.remove(new Entry(word,null));
	}
	
	/**
	 * Pobranie rozmiaru bazy
	 * @return zwraca ilość haseł
	 */
	public int getSize() { return dict.size(); }
	
	/**
	 * Zapis bazy danych do pliku
	 * @param fname nazwa pliku do zapisu
	 * @throws IOException błędy związane z plikiem
	 */
	public void saveDB(String fname) throws IOException{
		File file=new File(fname);
		PrintWriter writer=new PrintWriter(file.getAbsoluteFile());
		try {
			for(Entry e: dict) 
				writer.write(e.getWord()+"\n"+e.getClue()+"\n");
		}
		finally { 					//zapewnienie zamknięcia pliku
			writer.close(); 		//tutaj może dojść do przesłonięcia wyjątku jeśli close coś wyrzuci
		}
	}
	
	/**
	 * Tworzenie bazy danych na podstawie pliku
	 * @param fname nazwa pliku z danymi
	 * @throws IOException błędy związane z plikiem
	 */
	protected void createDB(String fname) throws IOException{
		File file=new File(fname);
		BufferedReader reader=new BufferedReader(new FileReader(file.getAbsoluteFile()));
		try {
			String word, clue;
			while ((word = reader.readLine()) != null) {
				clue = reader.readLine();
				if (clue == null)	//jest hasło ale nie ma wskazówki => jest to błąd pliku
					throw new IOException("Zły format pliku");
				add(word, clue);
			}
		} finally { 				//zapewnienie zamknięcia pliku
			reader.close(); 		//tutaj może dojść do przesłonięcia wyjątku jeśli close coś wyrzuci
		}
	}
}

