package crossword.dictionary;

import java.io.Serializable;

/**
 * Przechowuje hasło oraz wskazówkę do hasła
 * @author Florek
 *
 */
public class Entry implements Serializable {
	private String word;
	private String clue;
	
	/**
	 * Konstruktor
	 * @param word hasło
	 * @param clue wskazówka
	 */
	public Entry(String word, String clue) {
		this.word=word;
		this.clue=clue;
	}
	/**
	 * Pobieranie hasła
	 * @return zwraca hasło
	 */
	public String getWord() {
		return word;
	}
	/**
	 * Pobierania wskazówki
	 * @return zwraca wskazówkę
	 */
	public String getClue() {
		return clue;
	}
	/**
	 * Porównanie dwóch wpisów na podstawie haseł (case insensitive)
	 */
	public boolean equals(Object o) {
		if(o instanceof Entry) {
			Entry e=(Entry)o;
			return e.word.equalsIgnoreCase(word);
		}
		return false;
	}
}

