package crossword.board;

/**
 * Wyliczenie służące do określania kierunku
 * @author Florek
 *
 */
public enum Direction {
	VERT, HORIZ
}
