package crossword.board;

/**
 * Wyjątek generowany przez klasy z pakiety board
 * @author Florek
 *
 */
@SuppressWarnings("serial")
public class BoardException extends RuntimeException {
	public BoardException(String msg) {
		super(msg);
	}
}
