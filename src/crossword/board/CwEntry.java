package crossword.board;

import java.io.Serializable;
import crossword.dictionary.Entry;

/**
 * Klasa przechowująca informacje o wpisach z bazy danych które znajdują się na planszy krzyżówki
 * @see crossword.dictionary.Entry
 * @author Florek
 *
 */
public class CwEntry extends Entry implements Serializable{
	/** miejsce położenia x-wysokość, y-szerokość (jak macierz) */
	private int x,y;
	/** kierunek hasła */
	private Direction d;
	/**
	 * Konstruktor inicjalizujący wpis
	 * @param word hasło
	 * @param clue wskazówka
	 * @param x wysokość
	 * @param y szerokość
	 * @param direction kierunek
	 */
	public CwEntry(String word, String clue, int x, int y, Direction direction) {
		super(word, clue);
		this.x=x; this.y=y;
		d=direction;
	}
	/**
	 * Pobranie wysokości
	 * @return wysokość
	 */
	public int getX() { return x; }
	/**
	 * Pobranie szerkości
	 * @return szerokość
	 */
	public int getY() { return y; }
	/**
	 * pobranie kierunku
	 * @return kierunek
	 */
	public Direction getDir() { return d; } 
}
