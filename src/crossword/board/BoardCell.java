package crossword.board;

import java.io.Serializable;

/**
 * Klasa reprezentująca jedną komórkę na planszy
 * @author Florek
 *
 */
public class BoardCell implements Serializable {
	/**
	 * Klasa przechowująca informacje o komórce (zagnieżdrzona i prywatna)
	 */
	private static class CellInfo implements Serializable { 
		boolean horizStart, vertStart;
		boolean horizInner, vertInner;
		boolean horizEnd, vertEnd;
		/** Konstruktor dodany z powodu konstruktora kopiującego */
		CellInfo() {}
		/** Konstruktor kopiujący */
		CellInfo(CellInfo rv) {
			horizStart=rv.horizStart; horizEnd=rv.horizEnd; horizInner=rv.horizInner;
			vertStart=rv.vertStart; vertEnd=rv.vertEnd; vertInner=rv.vertInner;
		}
	}
	
	/** zawartość komórki*/
	private String content;
	/** dane o komórce*/
	private CellInfo info=new CellInfo();
	
	/**
	 * Konstruktor dodany w celu symetrii z kopiującym
	 */
	public BoardCell() {}
	
	/** 
	 * Konstruktor kopiujący tworzy kopie komórki
	 * @param rv komórka do skopiowania
	 * */
	public BoardCell(BoardCell rv) {
		content=rv.content;  		//zawartości nie będe kopiował bo String jest niemodyfikowalny więc nic złego się nie stanie
		info=new CellInfo(rv.info); //dane o komórce są kopiowanie
	}
	
	/**
	 * Ustawianie zawartości komórki
	 * @param content nowa zawartość
	 */
	public void setContent(String content) { this.content=content; }
	
	/**
	 * Pobieranie zawartości komórki
	 * @return zawartość komórki
	 */
	public String getContent() { return content; }
	
	//nazwy mówią wszystko
	public void enableHorizStart() { info.horizStart=true; }
	public void enableHorizInner() { info.horizInner=true; }
	public void enableHorizEnd() { info.horizEnd=true; }
	public void disableHorizStart() { info.horizStart=false; }
	public void disableHorizInner() { info.horizInner=false; }
	public void disableHorizEnd() { info.horizEnd=false; }
	public void disableHoriz() {
		disableHorizStart();
		disableHorizEnd();
		disableHorizInner();
	}

	public void enableVertStart() { info.vertStart=true; }
	public void enableVertInner() { info.vertInner=true; }
	public void enableVertEnd() { info.vertEnd=true; }
	public void disableVertStart() { info.vertStart=false; }
	public void disableVertInner() { info.vertInner=false; }
	public void disableVertEnd() { info.vertEnd=false; }
	public void disableVert() {
		disableVertStart();
		disableVertEnd();
		disableVertInner();
	}
	
	public boolean isHorizStart() { return info.horizStart; }
	public boolean isHorizInner() { return info.horizInner; }
	public boolean isHorizEnd() { return info.horizEnd; }
	public boolean isVertStart() { return info.vertStart; }
	public boolean isVertInner() { return info.vertInner; }
	public boolean isVertEnd() { return info.vertEnd; }
}
