package crossword.board;

import java.util.*;
import java.io.Serializable;

/**
 * Klasa reprezentująca planszę krzyżówki
 * @author Florek
 *
 */

public class Board implements Serializable {
	/** tablica planszy*/
	private BoardCell[][] board;
	/** wymiary (bardziej poręczne niż korzystanie z length)*/
	private int width, height;
	
	/**
	 * Kontruktor tworzący plansz i wstępnie inicjalizujący ustawienia pól
	 * @param height wysokość
	 * @param width szerokość
	 */
	public Board(int height, int width) {
		this.width=width;
		this.height=height;
		board=new BoardCell[height][width];
	
		//wstępne ustawienia planszy (min długość słowa=2)
		for(int i=0; i<height; i++)
			for(int j=0; j<width; j++) {
				//inicjalizuje każdą komórkę
				board[i][j]=new BoardCell();
				if(j!=0) board[i][j].enableHorizEnd(); 
				if(j!=width-1) board[i][j].enableHorizStart();
				if(j!=width-1 && j!=0) board[i][j].enableHorizInner();

				if(i!=0) board[i][j].enableVertEnd(); 
				if(i!=height-1) board[i][j].enableVertStart();
				if(i!=height-1 && i!=0) board[i][j].enableVertInner();
			}
	}
	
	/**
	 * Konstruktor kopijący tworzy kopię planszy
	 * @param b plansza do skopiowania
	 */
	public Board(Board b) { 
		width=b.width; height=b.height;
		board=new BoardCell[height][width];
		for(int i=0; i<height; i++)
			for(int j=0; j<width; j++)
				board[i][j]=new BoardCell(b.board[i][j]);
	}
	
	/**
	 * Zwraca szerokość planszy
	 * @return szerokość planszy
	 */
	public int getWidth() { return width; }
	
	/**
	 * Zwraca wysokość planszy
	 * @return wysokość planszy
	 */
	public int getHeight() { return height; }
	
	/**
	 * Zwraca zadaną komórkę
	 * @param x wysokość
	 * @param y szerokość
	 * @return wybrana komórka
	 */
	public BoardCell getCell(int x, int y) { return board[x][y]; }
	
	/**
	 * Ustawia zadaną komórkę
	 * @param x wysokość
	 * @param y szerokość
	 * @param c referencja do komórki
	 */
	public void setCell(int x, int y, BoardCell c) { board[x][y]=c; }
	
	/**
	 * Zwraca wszystkie komórki które mogą być początkowymi
	 * @return lista początkowych komórek
	 */
	public LinkedList<BoardCell> getStartCells() { 
		LinkedList<BoardCell> startCells=new LinkedList<BoardCell>();
		for(int i=0; i<height; i++)
			for(int j=0; j<width; j++)
				if(board[i][j].isHorizStart() || board[i][j].isVertStart())
					startCells.add(board[i][j]);
		return startCells;
	} 
	
	/**
	 * Tworzenie wzorca w celu wyszukania hasła pasującego do danego miejsca w krzyżówce
	 * @param fromx początkowy x
	 * @param fromy początkowy y
	 * @param tox końcowy x
	 * @param toy końcowy y
	 * @return wzorzec opisujący zadany obszar
	 */
	public String createPattern(int fromx, int fromy, int tox, int toy) throws BoardException{
		StringBuilder sb=new StringBuilder();
		sb.append("^");
		if(fromx==tox)
			for(int i=fromy; i<=toy; i++)
				if(board[fromx][i].getContent()==null)
					sb.append('.');
				else
					sb.append(board[fromx][i].getContent());
		else if(fromy==toy)
			for(int i=fromx; i<=tox; i++)
				if(board[i][fromy].getContent()==null)
					sb.append('.');
				else
					sb.append(board[i][fromy].getContent());
		else
			throw new BoardException("Złe wartość opisujące obszar do utworzenia regexa");
		sb.append('$');
		return sb.toString().toLowerCase();
	}
}

