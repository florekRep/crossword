package crossword.logic;

import java.util.*;
import java.io.*;
import crossword.board.*;
import crossword.dictionary.*;
import crossword.strategy.*;

/**
 * Klasa odpowiada za logikę działąnia krzyżówki
 * @author Florek
 *
 */
public class Crossword {
	/** lista haseł znajdujących się w krzyżówce */
	private LinkedList<CwEntry> entries=new LinkedList<CwEntry>();
	/** plansza krzyżówki */
	private Board b;
	/** baza z hasłami */
	private transient InteliCwDB cwdb;
	/** identyfikator krzyżówki */
	private transient final long ID;
	
	/** 
	 * Konstruktor ustawiający id wykorzystywany przy deserializacji
	 * @param id numer
	 */
	public Crossword(long id) {
		ID=id;
	}
	/**
	 * Konstruktor tworzący krzyżówkę
	 * @param height wysokość
	 * @param width szerokość
	 * @param initDB początkowa baza danych
	 */
	public Crossword(int height, int width, InteliCwDB initDB) {
		ID=-1;
		cwdb=initDB;
		b=new Board(height, width);
	}
	
	public long getID() {
		return ID;
	}
	
	/**
	 * Zwraca iterator tylko do odczytu, do przeglądania haseł leżących w krzyżówce
	 * @return Read Only Iterator
	 */
	public Iterator<CwEntry> getROEntryIter() {
		return Collections.unmodifiableList(entries).iterator(); //powinno działać (??)
	}
	
	/**
	 * Zwraca kopie planszy
	 * @return kopia planszy
	 */
	public Board getBoardCopy() {
		return new Board(b);
	}
	/**
	 * Zwraca bazę danych
	 * @return baza danych
	 */
	public InteliCwDB getCwDB() { return cwdb; }
	/**
	 * Ustawia bazę danych
	 * @param cwdb nowa baza danych
	 */
	public void setCwDB(InteliCwDB cwdb) { this.cwdb=cwdb; }
	/**
	 * Sprawdza czy hasło już jest w krzyżówce
	 * @param word hasło
	 * @return true jeśli już się znajduje, false jeśli nie
	 */
	public boolean contains(String word) {
		return entries.contains(new Entry(word, null));
	}
	
	/**
	 * Dodanie nowego wpisu do krzyżówki
	 * @param cwe wpis
	 * @param s sposób dodania
	 */
	public final void addCwEntry(CwEntry cwe, Strategy s) {
	  entries.add(cwe);
	  s.updateBoard(b,cwe);
	}
	
	/**
	 * Generowanie krzyżówki
	 * @param s sposób generowania
	 * @throws Exception
	 */
	public final void generate(Strategy s) throws BoardException{
	  CwEntry e = null;
	  while((e = s.findEntry(this)) != null){
	    addCwEntry(e,s);
	  }
	}
	
	/**
	 * Metoda służąca do serializacji crossword
	 * @param oos strumień 
	 * @throws IOException błędy strumienia
	 */
	public void serializeCw(ObjectOutputStream oos) throws IOException{
		oos.writeObject(b);
		oos.writeObject(entries);
	}
	
	/**
	 * Metoda służąca do deserializacji crossword
	 * @param ois strumień
	 * @throws IOException błędy strumienia
	 * @throws ClassNotFoundException błędy strumienia
	 */
	public void deserializeCw(ObjectInputStream ois) throws IOException, ClassNotFoundException {
		b=(Board)ois.readObject();
		entries=(LinkedList<CwEntry>)ois.readObject();
	}
}

