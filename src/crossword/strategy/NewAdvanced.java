package crossword.strategy;

import java.util.*;
import java.util.regex.*;
import crossword.board.*;
import crossword.logic.*;
import crossword.dictionary.*;

public class NewAdvanced extends Strategy {
	
	private Random rand=new Random();
	private int ct=0;

	private int getRandom(int n) {
		if(n==0) return 0;
		return rand.nextInt(n);
	}
	
	private static class DataCube {
		public int firstLetter=0, lastLetter=0;
		public String pattern;
	}
	
	private DataCube trimPattern(final String pattern) {
		DataCube dc=new DataCube();
		dc.firstLetter=getRandom(pattern.length()-2);
		dc.lastLetter=dc.firstLetter;
		int letterIndex=pattern.length()/2;
		int k;
		for(k=pattern.length()-2; k>0; k--) 
			if(pattern.charAt(k)!='.') {
				letterIndex=k-1;
				break;
			}
		int limit=letterIndex;
		int start;
		if(limit<15)
			start=getRandom(limit);
		else
			start=limit-15+getRandom(15);
		for(k=start+1; k<pattern.length()-1; k++) 
			if(pattern.charAt(k)!='.') {
				letterIndex=k-1;
				break;
			}

		limit=pattern.length()-letterIndex-2;
		//maks długość regexpa nie większa niż 15 liter 
		if(limit+(letterIndex-start) > 15)
			limit=15-(letterIndex-start);
		int end=letterIndex+getRandom(limit);
	
		int counter=0;
		k=1;
		while(k<pattern.length()-1 && (k<start+1 || pattern.charAt(k)=='.')) {
			if(pattern.charAt(k)=='.')
				counter++;
			else
				counter=0;
			k++;
		}
		if(k!=pattern.length()-1)
			dc.firstLetter=k-1;
		StringBuilder trimmedPattern=new StringBuilder(pattern);
		trimmedPattern.replace(0,k,"^.{0,"+counter+"}");
		if(k!=pattern.length()-1) {
			counter=0;
			k=1;
			while(pattern.length()-1-k>0 && (pattern.length()-1-k>end+1
				|| pattern.charAt(pattern.length()-1-k)=='.')) {
				if(pattern.charAt(pattern.length()-1-k)=='.')
					counter++;
				else
					counter=0;
				k++;
			}
			dc.lastLetter=pattern.length()-2-k;
			trimmedPattern.replace(trimmedPattern.length()-k, trimmedPattern.length(), ".{0,"+counter+"}$");
		}
		dc.pattern=trimmedPattern.toString();
		return dc;
	}
	private boolean matches(Board b, int x, int y, String word, Direction dir) {
		if(dir==Direction.HORIZ) 
			for(int i=y; i<y+word.length(); i++) {
				if(b.getCell(x, i).getContent()!=null && !b.getCell(x, i).getContent().equals(word.substring(i-y, i-y+1))) 
					return false;
			}
		else 
			for(int i=x; i<x+word.length(); i++) 
				if(b.getCell(i, y).getContent()!=null && !b.getCell(i, y).getContent().equals(word.substring(i-x,i-x+1))) 
					return false;
		return true;
	}
	
	private CwEntry findEntryVert(Crossword cw, Board b) throws BoardException{
		Map<Integer,List<Integer[]>> startCells=new TreeMap<Integer,List<Integer[]>>(Collections.reverseOrder());
		//generowanie możliwych startowych opcji
		for(int i=0; i<b.getWidth(); i++) {
			int start=0;
			int end=-1;
			int counter=0;
			for(int j=0; j<b.getHeight(); j++) {
				if(b.getCell(j, i).isVertEnd()) //czy komórka może być końce
					end=j; //jeśli tak to update końca
				if(start==j) { //jeśli ustawiono nowy start to sprawdzamy czy jest poprawny
					if(b.getCell(j, i).isVertStart() && b.getCell(j,i).getContent()!=null)
						counter++;
					else if(!b.getCell(j, i).isVertStart())
						start=j+1; //jeśli nie to przejdźmy do następnej komórki
				}
				else if(b.getCell(j, i).isVertInner()) {//napotkana może być wewnętrzną, ok
					if(b.getCell(j, i).getContent()!=null)
						counter++;
				}
				else { //nie może być wewnętrzną, trzeba kończyć pasek
					if(b.getCell(j, i).getContent()!=null)
						counter++;
					if(end!=-1 && (end-start)>=1) { //jeśli jego długość >2 to dodamy do mapy
						if(startCells.get(counter)==null)
							startCells.put(counter, new LinkedList<Integer[]>());
						startCells.get(counter).add(new Integer[]{i, start, end});
					}
					counter=0;
					start=j;
					j--; //ponowne rozpatrzenie pola (może być zbędne w sumie)
				}
			}
		}
		
		//wg priorytetów od tych co przecinają najwięcej
		for(Integer i: startCells.keySet()) {
			List<Integer[]> lista=startCells.get(i);
			for(int j=0; j<3; j++) {
				Integer[] starts=lista.get(rand.nextInt(lista.size())); //losowanie przykładowe miejsca 
				//stworzenie wzorca do wyszukania o losowej długości (może dodam kilka prób na utworzenie go )
				top:
				for(int c=0; c<3; c++) {	
					String pattern=b.createPattern(starts[1], starts[0], starts[2], starts[0]);
					if(pattern.replaceAll("\\.", "").equals("^$") && ct!=1)
						return null;
					DataCube dc=trimPattern(pattern);
					//3 opcje na wyszukanie
					outer: 
					for(int k=0; k<3; k++) {
						//wyszukanie pasującego do patternu
						Entry entry=cw.getCwDB().getRandom(dc.pattern);
						if(entry==null) //nie ma takiego
							continue top;
						String word=entry.getWord();
						Pattern p=Pattern.compile(pattern.substring(dc.firstLetter+1, dc.lastLetter+2), Pattern.CASE_INSENSITIVE);
						Matcher m=p.matcher(word);
						while(m.find()) {
							int wordIndex=m.start();
							if(starts[1]+dc.firstLetter-wordIndex>=b.getHeight() || starts[1]+dc.firstLetter-wordIndex<0
								|| starts[1]+dc.firstLetter-wordIndex+word.length()-1<0
								|| starts[1]+dc.firstLetter-wordIndex+word.length()-1>=b.getHeight() 
								|| !b.getCell(starts[1]+dc.firstLetter-wordIndex, starts[0]).isVertStart() 
								|| !b.getCell(starts[1]+dc.firstLetter-wordIndex+word.length()-1, starts[0]).isVertEnd()
								|| !matches(b,starts[1]+dc.firstLetter-wordIndex,starts[0],word,Direction.VERT))
								continue;
						
							for(Iterator<CwEntry> iter=cw.getROEntryIter(); iter.hasNext(); ) //nie może się powtórzyć
								if(iter.next().getWord().equalsIgnoreCase(word))
									continue outer;
							return new CwEntry(entry.getWord(), entry.getClue(),starts[1]+(dc.firstLetter-wordIndex), starts[0], Direction.VERT);
						}
					}
				}
			}
		}
		return null;
	}
	private CwEntry findEntryHoriz(Crossword cw, Board b) throws BoardException{
		Map<Integer,List<Integer[]>> startCells=new TreeMap<Integer,List<Integer[]>>(Collections.reverseOrder());
		//generowanie możliwych startowych opcji
		for(int i=0; i<b.getHeight(); i++) {
			int start=0;
			int end=-1;
			int counter=0;
			for(int j=0; j<b.getWidth(); j++) {
				if(b.getCell(i, j).isHorizEnd()) //czy komórka może być końce
					end=j; //jeśli tak to update końca
				if(start==j) {//jeśli ustawiono nowy start to sprawdzamy czy jest poprawny
					if(b.getCell(i, j).isHorizStart() && b.getCell(i, j).getContent()!=null)
						counter++;
					if(!b.getCell(i, j).isHorizStart())
						start=j+1; //jeśli nie to przejdźmy do następnej komórki
				}
				else if(b.getCell(i, j).isHorizInner()) {//napotkana może być wewnętrzną, ok
					if(b.getCell(i, j).getContent()!=null)
						counter++;
				}
				else { //nie może być wewnętrzną, trzeba kończyć pasek
					if(b.getCell(i, j).getContent()!=null)
						counter++;
					if(end!=-1 && (end-start)>=1) { //jeśli jego długość >2 to dodamy do mapy
						if(startCells.get(counter)==null)
							startCells.put(counter, new LinkedList<Integer[]>());
						startCells.get(counter).add(new Integer[]{i, start, end});
					}
					counter=0;
					start=j;
					j--; //ponowne rozpatrzenie pola (może być zbędne w sumie)
				}
			}
		}
		
		for(Integer i: startCells.keySet()) {
			List<Integer[]> lista=startCells.get(i);
			for(int j=0; j<3; j++) {
				Integer[] starts=lista.get(getRandom(lista.size())); //losowanie przykładowe miejsca 
				
				top:
				for(int c=0; c<3; c++) {
					String pattern=b.createPattern(starts[0], starts[1], starts[0], starts[2]);
					if(pattern.replaceAll("\\.", "").equals("^$") && ct!=1)
						continue ;
					DataCube dc=trimPattern(pattern);
					//3 opcje na wyszukanie
					outer: 
					for(int k=0; k<3; k++) {
						//wyszukanie pasującego do patternu
						Entry entry=cw.getCwDB().getRandom(dc.pattern);
						if(entry==null) //nie ma takiego
							continue top;
						String word=entry.getWord();
						Pattern p=Pattern.compile(pattern.substring(dc.firstLetter+1, dc.lastLetter+2), Pattern.CASE_INSENSITIVE);
						Matcher m=p.matcher(word);
						while(m.find()) {
							int wordIndex=m.start();
							if(starts[1]+dc.firstLetter-wordIndex>=b.getWidth() || starts[1]+dc.firstLetter-wordIndex<0
								|| starts[1]+dc.firstLetter-wordIndex+word.length()-1>=b.getWidth() 
								|| starts[1]+dc.firstLetter-wordIndex+word.length()-1<0
								|| !b.getCell(starts[0], starts[1]+dc.firstLetter-wordIndex).isHorizStart() 
								|| !b.getCell(starts[0], starts[1]+dc.firstLetter-wordIndex+word.length()-1).isHorizEnd()
								|| !matches(b,starts[0],starts[1]+dc.firstLetter-wordIndex,word,Direction.HORIZ)
								)
							continue;
							for(Iterator<CwEntry> iter=cw.getROEntryIter(); iter.hasNext(); ) //nie może się powtórzyć
								if(iter.next().getWord().equalsIgnoreCase(word))
									continue outer;
							return new CwEntry(entry.getWord(), entry.getClue(), starts[0], starts[1]+dc.firstLetter-wordIndex, Direction.HORIZ);
						}
					}
				}
			}
		}
		return null;
	}
	
	//works fine
	@Override
	public CwEntry findEntry(Crossword cw) throws BoardException{
		ct++;
		Board b=cw.getBoardCopy();
		CwEntry result;
		if(getRandom(1)==0) {
			if((result=findEntryVert(cw,b))==null)
				result=findEntryHoriz(cw,b);
		}
		else {
			if((result=findEntryHoriz(cw,b))==null)
				result=findEntryVert(cw,b);
		}
		return result;
	}
	
	
	//works fine
	@Override
	/*
	 * Update board polega na wpisaniu dodawanago hasła w odpowiednie miejsca na planszy +
	 * aktualizacja właściwości okienek w około 
	 * (non-Javadoc)
	 * @see Strategy#updateBoard(Board, CwEntry)
	 */
	public void updateBoard(Board b, CwEntry e) throws WordPlacementException{
		if(e.getDir()==Direction.VERT) {
			for(int i=e.getX(); i<e.getX()+e.getWord().length(); i++) {
				if(b.getCell(i, e.getY()).getContent()==null)
					b.getCell(i, e.getY()).setContent(e.getWord().substring(i-e.getX(), i-e.getX()+1));	//pisanie litery hasła
				else if(!b.getCell(i, e.getY()).getContent().equalsIgnoreCase(e.getWord().substring(i-e.getX(), i-e.getX()+1)))
					throw new WordPlacementException("hasło nie zazębia się");
				b.getCell(i, e.getY()).disableVert();
				if(e.getY()-1>=0) {
					b.getCell(i, e.getY()-1).disableVert();
					b.getCell(i, e.getY()-1).disableHorizEnd();
				}
				if(e.getY()+1<b.getWidth()) {
					b.getCell(i, e.getY()+1).disableVert();
					b.getCell(i, e.getY()+1).disableHorizStart();
				}
			}
			if(e.getX()-1>=0) {
				b.getCell(e.getX()-1, e.getY()).disableHoriz();
				b.getCell(e.getX()-1, e.getY()).disableVert();
				if(e.getY()-1>=0) {
					b.getCell(e.getX()-1, e.getY()-1).disableHoriz();
					b.getCell(e.getX()-1, e.getY()-1).disableVert();
				}
				if(e.getY()+1<b.getWidth()) {
					b.getCell(e.getX()-1, e.getY()+1).disableHoriz();
					b.getCell(e.getX()-1, e.getY()+1).disableVert();
				}
			}
			if(e.getX()+e.getWord().length()<b.getHeight()) {
				b.getCell(e.getX()+e.getWord().length(), e.getY()).disableHoriz();
				b.getCell(e.getX()+e.getWord().length(), e.getY()).disableVert();
				if(e.getY()-1>=0) {
					b.getCell(e.getX()+e.getWord().length(), e.getY()-1).disableHoriz();
					b.getCell(e.getX()+e.getWord().length(), e.getY()-1).disableVert();
				}
				if(e.getY()+1<b.getWidth()) {
					b.getCell(e.getX()+e.getWord().length(), e.getY()+1).disableHoriz();
					b.getCell(e.getX()+e.getWord().length(), e.getY()+1).disableVert();
				}
			}
		} else {
			for(int i=e.getY(); i<e.getY()+e.getWord().length(); i++) {
				if(b.getCell(e.getX(), i).getContent()==null)
					b.getCell(e.getX(), i).setContent(e.getWord().substring(i-e.getY(), i-e.getY()+1)); //pisanie litery hasła
				else if(!b.getCell(e.getX(), i).getContent().equalsIgnoreCase(e.getWord().substring(i-e.getY(), i-e.getY()+1)))
					throw new WordPlacementException("hasło nie zazębia się");
				b.getCell(e.getX(), i).disableHoriz();
				if(e.getX()-1>=0) {
					b.getCell(e.getX()-1, i).disableHoriz();
					b.getCell(e.getX()-1, i).disableVertEnd();
				}
				if(e.getX()+1<b.getHeight()) {
					b.getCell(e.getX()+1, i).disableHoriz();
					b.getCell(e.getX()+1, i).disableVertStart();
				}
			}
			if(e.getY()-1>=0) {
				b.getCell(e.getX(), e.getY()-1).disableHoriz();
				b.getCell(e.getX(), e.getY()-1).disableVert();
				if(e.getX()-1>=0) {
					b.getCell(e.getX()-1, e.getY()-1).disableHoriz();
					b.getCell(e.getX()-1, e.getY()-1).disableVert();
				}
				if(e.getX()+1<b.getHeight()) {
					b.getCell(e.getX()+1, e.getY()-1).disableHoriz();
					b.getCell(e.getX()+1, e.getY()-1).disableVert();
				}
			}
			if(e.getY()+e.getWord().length()<b.getWidth()) {
				b.getCell(e.getX(), e.getY()+e.getWord().length()).disableHoriz();
				b.getCell(e.getX(), e.getY()+e.getWord().length()).disableVert();
				if(e.getX()-1>=0) {
					b.getCell(e.getX()-1, e.getY()+e.getWord().length()).disableHoriz();
					b.getCell(e.getX()-1, e.getY()+e.getWord().length()).disableVert();
				}
				if(e.getX()+1<b.getHeight()) {
					b.getCell(e.getX()+1, e.getY()+e.getWord().length()).disableHoriz();
					b.getCell(e.getX()+1, e.getY()+e.getWord().length()).disableVert();
				}
			}
		}
	}

}

