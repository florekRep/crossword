package crossword.strategy;

import java.util.*;
import crossword.board.*;
import crossword.logic.*;
import crossword.dictionary.*;

/**
 * Generator zaawansowanych krzyżówek (hehe)
 * @author Florek
 *
 */
public class Advanced extends Strategy {
	private Random rand=new Random();
	/** licznik wykonań funkcji */
	private int ct=0;
	/** funkcja random zmieniona na własny użytek */
	private int getRandom(int n) {
		if(n==0) return 0;
		return rand.nextInt(n);
	}
	/** dodawanie hasła pionowo  */
	private CwEntry findEntryVert(Crossword cw, Board b) throws BoardException {
		Map<Integer,List<Integer[]>> startCells=new TreeMap<Integer,List<Integer[]>>(Collections.reverseOrder());
		for(int i=0; i<b.getWidth(); i++) {													//generowanie możliwych startowych opcji
			int start=0;
			int end=-1;
			int counter=0;
			for(int j=0; j<b.getHeight(); j++) {
				if(b.getCell(j, i).isVertEnd()) 											//czy komórka może być końce
					end=j; 																	//jeśli tak to update końca
				if(start==j) { 																//jeśli ustawiono nowy start to sprawdzamy czy jest poprawny
					if(b.getCell(j, i).isVertStart() && b.getCell(j,i).getContent()!=null)
						counter++;
					else if(!b.getCell(j, i).isVertStart())
						start=j+1; 															//jeśli nie to przejdźmy do następnej komórki
				}
				else if(b.getCell(j, i).isVertInner()) {								  	//napotkana może być wewnętrzną, ok
					if(b.getCell(j, i).getContent()!=null)
						counter++;
				}
				else { 																		//nie może być wewnętrzną, trzeba kończyć pasek
					if(b.getCell(j, i).getContent()!=null)
						counter++;
					if(end!=-1 && (end-start)>=1) { 										//jeśli jego długość >2 to dodamy do mapy
						if(startCells.get(counter)==null)
							startCells.put(counter, new LinkedList<Integer[]>());
						startCells.get(counter).add(new Integer[]{i, start, end});
					}
					counter=0;
					start=j;
					j--; 																	//ponowne rozpatrzenie pola
				}
			}
		}
		
		for(Integer i: startCells.keySet()) {											 	//wg priorytetów od tych co przecinają najwięcej
			List<Integer[]> lista=startCells.get(i);
			top:
			for(int j=0; j<3; j++) {
				Integer[] starts=lista.get(rand.nextInt(lista.size())); 					//losowanie przykładowego miejsca 
				outer:
				for(int c=0; c<5; c++) {													//stworzenie wzorca do wyszukania o losowej długości 
					String pattern=b.createPattern(starts[1], starts[0], starts[2], starts[0]);
					
					if(pattern.replaceAll("\\.", "").equals("^$") && ct!=1)					//jeśli pusty oraz nie jest to piewsze wykonanie funkcji
						continue top;
					
					int letterIndex=(starts[1]+starts[2])/2-starts[1];						//wylosowanie długości hasła tak aby zawierało
					int k;																	//przynajmniej jedną literę z linii
					for(k=pattern.length()-2; k>0; k--) 									//oraz jego długość < 15
						if(pattern.charAt(k)!='.') {
							letterIndex=k-1;
							break;
						}
					int limit=letterIndex;
					int start;
					if(limit<15)
						start=starts[1]+getRandom(limit);
					else
						start=starts[1]+limit-15+getRandom(15);
					
					for(k=start+1-starts[1]; k<pattern.length()-1; k++) 
						if(pattern.charAt(k)!='.') {
							letterIndex=k-1;
							break;
						}
					limit=pattern.length()-letterIndex-2;
					if(limit+(letterIndex-start) > 15)
						limit=15-(letterIndex-start);
					int end=starts[1]+letterIndex+getRandom(limit);
																						//sprawdzenie czy komórki mogą być startowe/końcowe
					if(!b.getCell(start, starts[0]).isVertStart() || !b.getCell(end, starts[0]).isVertEnd() || (end-start)<1)
						continue;
					
					pattern=b.createPattern(start,starts[0], end, starts[0]);			//utworzneie nowego patternu o wylosowanej długości
					
					inner: 
					for(k=0; k<3; k++) {
						Entry entry=cw.getCwDB().getRandom(pattern);					//wyszukanie pasującego do patternu
						if(entry==null) 												//nie ma takiego
							continue outer;
						String word=entry.getWord();
						if(cw.contains(word))											//nie może się powtórzyć
							continue inner;	
						return new CwEntry(entry.getWord(), entry.getClue(),start, starts[0], Direction.VERT);
					}
				}
			}
		}
		return null;
	}
	
	/** wybranie hasła poziomego */
	private CwEntry findEntryHoriz(Crossword cw, Board b) throws BoardException{
		Map<Integer,List<Integer[]>> startCells=new TreeMap<Integer,List<Integer[]>>(Collections.reverseOrder());
		for(int i=0; i<b.getHeight(); i++) {											//generowanie możliwych startowych opcji
			int start=0;
			int end=-1;
			int counter=0;
			for(int j=0; j<b.getWidth(); j++) {
				if(b.getCell(i, j).isHorizEnd()) 										//czy komórka może być końce
					end=j; 																//jeśli tak to update końca
				if(start==j) 															{//jeśli ustawiono nowy start to sprawdzamy czy jest poprawny
					if(b.getCell(i, j).isHorizStart() && b.getCell(i, j).getContent()!=null)
						counter++;
					if(!b.getCell(i, j).isHorizStart())
						start=j+1; 														//jeśli nie to przejdźmy do następnej komórki
				}
				else if(b.getCell(i, j).isHorizInner()) {								//napotkana może być wewnętrzną, ok
					if(b.getCell(i, j).getContent()!=null)
						counter++;
				}
				else { 																	//nie może być wewnętrzną, trzeba kończyć pasek
					if(b.getCell(i, j).getContent()!=null)
						counter++;
					if(end!=-1 && (end-start)>=1) { 									//jeśli jego długość >2 to dodamy do mapy
						if(startCells.get(counter)==null)
							startCells.put(counter, new LinkedList<Integer[]>());
						startCells.get(counter).add(new Integer[]{i, start, end});
					}
					counter=0;
					start=j;
					j--; 																//ponowne rozpatrzenie pola
				}
			}
		}
		
		for(Integer i: startCells.keySet()) {											//wg priorytetów od tych co przecinają najwięcej
			List<Integer[]> lista=startCells.get(i);
			top:
			for(int j=0; j<3; j++) {
				Integer[] starts=lista.get(getRandom(lista.size())); 					//losowanie przykładowe miejsca 
				outer:
				for(int c=0; c<5; c++) {												//stworzenie wzorca do wyszukania o losowej długości 
					String pattern=b.createPattern(starts[0], starts[1], starts[0], starts[2]);
					
					if(pattern.replaceAll("\\.", "").equals("^$") && ct!=1)				//jeśli pusty oraz nie jest to piewsze wykonanie funkcji
						continue top;
					
					int letterIndex=(starts[1]+starts[2])/2-starts[1];					//wylosowanie długości hasła tak aby zawierało
					int k;																//przynajmniej jedną literę z linii
					for(k=pattern.length()-2; k>0; k--) 								//oraz jego długość < 15
						if(pattern.charAt(k)!='.') {
							letterIndex=k-1;
							break;
						}
					int limit=letterIndex;
					int start;
					if(limit<15)
						start=starts[1]+getRandom(limit);
					else
						start=starts[1]+limit-15+getRandom(15);
					
					for(k=start+1-starts[1]; k<pattern.length()-1; k++) 
						if(pattern.charAt(k)!='.') {
							letterIndex=k-1;
							break;
						}
				
					limit=pattern.length()-letterIndex-2;
					if(limit+(letterIndex-start) > 15)
						limit=15-(letterIndex-start);
					int end=starts[1]+letterIndex+getRandom(limit);
																						//sprawdzenie czy komórki mogą być startowe/końcowe
					if(!b.getCell(starts[0], start).isHorizStart() || !b.getCell(starts[0], end).isHorizEnd() || (end-start)<1 || (end-start)>16)
						continue ;	
					
					pattern=b.createPattern(starts[0] ,start, starts[0], end);			//utworzneie nowego patternu o wylosowanej długości
					
					inner: 
					for(k=0; k<3; k++) {
						Entry entry=cw.getCwDB().getRandom(pattern);					//wyszukanie pasującego do patternu
						if(entry==null) //nie ma takiego
							continue outer;
						String word=entry.getWord();
						if(cw.contains(word))											//nie może się powtóżyć
							continue inner;
						return new CwEntry(entry.getWord(), entry.getClue(), starts[0], start, Direction.HORIZ);
					}
				}
			}
		}
		return null;
	}
	/**
	 * Dodaje na przemian raz pionowo, raz poziomo
	 * @see Strategy#findEntry(Crossword)
	 */
	@Override
	public CwEntry findEntry(Crossword cw) throws BoardException{
		ct++;
		Board b=cw.getBoardCopy();
		CwEntry result;
		if(ct%2==0) {
			if((result=findEntryVert(cw,b))==null)
				result=findEntryHoriz(cw,b);
		}
		else {
			if((result=findEntryHoriz(cw,b))==null)
				result=findEntryVert(cw,b);
		}
		return result;
	}
	
	/**
	 * Aktualizuje planszę
	 * @see Strategy#updateBoard(Board, CwEntry)
	 */
	@Override
	public void updateBoard(Board b, CwEntry e) throws WordPlacementException{
		if(e.getDir()==Direction.VERT) {																//Pionowo
			for(int i=e.getX(); i<e.getX()+e.getWord().length(); i++) {
				if(b.getCell(i, e.getY()).getContent()==null)
					b.getCell(i, e.getY()).setContent(e.getWord().substring(i-e.getX(), i-e.getX()+1));	//pisanie litery hasła
				else if(!b.getCell(i, e.getY()).getContent().equalsIgnoreCase(e.getWord().substring(i-e.getX(), i-e.getX()+1)))
					throw new WordPlacementException("hasło nie zazębia się");
				b.getCell(i, e.getY()).disableVert();													//kolumna z hasłem
				if(e.getY()-1>=0) {																		//kolumna na lewo od hasła
					b.getCell(i, e.getY()-1).disableVert();
					b.getCell(i, e.getY()-1).disableHorizEnd();
				}
				if(e.getY()+1<b.getWidth()) {															//kolumna na prawo od hasła
					b.getCell(i, e.getY()+1).disableVert();
					b.getCell(i, e.getY()+1).disableHorizStart();
				}
			}
			if(e.getX()-1>=0) {																			//komórka przed
				b.getCell(e.getX()-1, e.getY()).disableHoriz();
				b.getCell(e.getX()-1, e.getY()).disableVert();
			}
			if(e.getX()+e.getWord().length()<b.getHeight()) {											//komórka za
				b.getCell(e.getX()+e.getWord().length(), e.getY()).disableHoriz();
				b.getCell(e.getX()+e.getWord().length(), e.getY()).disableVert();
			}
		} else {																						//poziomo
			for(int i=e.getY(); i<e.getY()+e.getWord().length(); i++) {
				if(b.getCell(e.getX(), i).getContent()==null)
					b.getCell(e.getX(), i).setContent(e.getWord().substring(i-e.getY(), i-e.getY()+1)); //pisanie litery hasła
				else if(!b.getCell(e.getX(), i).getContent().equalsIgnoreCase(e.getWord().substring(i-e.getY(), i-e.getY()+1)))
					throw new WordPlacementException("hasło nie zazębia się");
				b.getCell(e.getX(), i).disableHoriz();													//wiersz z hasłem
				if(e.getX()-1>=0) {																		//wiersz nad
					b.getCell(e.getX()-1, i).disableHoriz();
					b.getCell(e.getX()-1, i).disableVertEnd();
				}
				if(e.getX()+1<b.getHeight()) {															//wiersz pod
					b.getCell(e.getX()+1, i).disableHoriz();
					b.getCell(e.getX()+1, i).disableVertStart();
				}
			}
			if(e.getY()-1>=0) {																			//komórka przed
				b.getCell(e.getX(), e.getY()-1).disableHoriz();
				b.getCell(e.getX(), e.getY()-1).disableVert();
			}
			if(e.getY()+e.getWord().length()<b.getWidth()) {											//komórka za
				b.getCell(e.getX(), e.getY()+e.getWord().length()).disableHoriz();
				b.getCell(e.getX(), e.getY()+e.getWord().length()).disableVert();
			}
		}
	}

}

