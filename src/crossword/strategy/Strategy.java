package crossword.strategy;

import crossword.board.*;
import crossword.logic.*;

/**
 * Klasa dostarczająca interfejsu dla algorytmów generujących krzyżówki
 * @author Florek
 *
 */
public abstract class Strategy {
	/**
	 * Wyszukiwanie następnego hasła do wpisania
	 * @param cw krzyżówka
	 * @return wpis zawięrający informacje o haśle i miejscu ulokowania
	 * @throws Exception to się jeszcze zrobi
	 */
	public abstract CwEntry findEntry(Crossword cw) throws BoardException;
	
	/**
	 * Aktualizuje wygląd planszy
	 * @param b plansza do aktualizacji
	 * @param e wpis do dodania
	 * @throws WordPlacementException słowo nie mogło być wpisane
	 */
	public abstract void updateBoard(Board b, CwEntry e) throws WordPlacementException;
}

