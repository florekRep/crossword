package crossword.strategy;

/**
 * Wyjątek pojawia się gdy wybrane hasło nie może zostać wpisane w plansze
 * (powinien to być raczej RuntimeException, bo do takiej sytuacji nie może dojść)
 * @author Florek
 *
 */
@SuppressWarnings("serial")
public class WordPlacementException extends RuntimeException {
	public WordPlacementException(String msg) {
		super(msg);
	}
}
