package crossword.strategy;

import crossword.board.*;
import crossword.logic.*;
import crossword.dictionary.*;

/**
 * Generator prostych krzyżówek
 * @author Florek
 *
 */
public class Simple extends Strategy {
	/** hasło będące rozwiązaniem krzyżówki */
	private String answer;
	/** licznik uruchomień metody */
	private int counter=0;
	
	/**
	 * Dodanie nasępnego hasła do krzyżówki
	 * @see Strategy#findEntry(Crossword)
	 */
	@Override
	public CwEntry findEntry(Crossword cw) {
		Board boardCopy=cw.getBoardCopy(); 			//utworznie kopii planszy (można zastąpić zwykłym pobraniem referencji)
		int answerIndex=(boardCopy.getWidth()-1)/2; //obliczenie miejsca gdzie będzie wpisywane hasło (na środku) 
		if(answer==null) {							//pierwsze uruchomienie wyznaczy również  hasło
			do {									//losowanie hasła którego wysokość jest mniejsza niż wysokość krzyżówki
				answer=cw.getCwDB().getRandom().getWord();
			} while(answer.length()>boardCopy.getHeight()); 
		}
		else if(answer.length()==counter) 			//jeśli już znaleziono wszystkie pasujące to koniec wykonania
			return null;
		
		Entry rnd=null;
		boolean good=false;
		String word=null;
		while(good!=true) {
													//losowanie hasła 
			rnd=cw.getCwDB().getRandom(".{0,"+answerIndex+"}"+answer.charAt(counter)
									+".{0,"+(boardCopy.getWidth()-answerIndex-1)+"}"); 	
			word=rnd.getWord();
													//nie może być takie samo jak hasło całej krzyżówki, ani też nie możę się powtrzać
			if(word.equalsIgnoreCase(answer) || cw.contains(word)) 
				continue;					 	    //powtórzenie wyszukiwania
			
													//sprawdzenie czy mieści się na planszy (trzeba sprawdzić bo użyłem {0,x} {0,y} 
			if((word.indexOf(answer.charAt(counter))>answerIndex) ||  
			   (word.length()-word.indexOf(answer.charAt(counter))>(boardCopy.getWidth()-answerIndex)))
			   		continue;
			good=true;
		}
		//obliczenie pozycji startowej dodawanego hasła
		int yStart=answerIndex-word.indexOf(answer.charAt(counter));
		int xStart=counter;
		counter++;
		
		return new CwEntry(rnd.getWord(), rnd.getClue(), xStart, yStart, Direction.HORIZ);
	}

	/**
	 * Aktualizacja planszy
	 * @see Strategy#updateBoard(Board, CwEntry)
	 */
	@Override
	public void updateBoard(Board b, CwEntry e) throws WordPlacementException{
		for(int i=e.getY(); i<e.getY()+e.getWord().length(); i++) {		//zwyczajne wpisanie hasła na plansze
			if(b.getCell(e.getX(), i).getContent()==null)
				b.getCell(e.getX(), i).setContent(e.getWord().substring(i-e.getY(), i-e.getY()+1));
			else if(!b.getCell(e.getX(), i).getContent().equalsIgnoreCase(e.getWord().substring(i-e.getY(), i-e.getY()+1)))
				throw new WordPlacementException("hasła nie zazębiają się");
		}
	}

}
