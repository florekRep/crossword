package GUI;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.prefs.Preferences;

import crossword.dictionary.*;
import crossword.strategy.*;

/**
 * Klasa odpowiedzialna za graficzny interfejs Generatora
 * @author Florek
 *
 */
public class Generate extends JPanel{
	private JLabel height=new JLabel("Wysokość");
	/** wybór rozmiaru */
	private JSpinner heightS=new JSpinner(new SpinnerNumberModel(11,3,51,1));
	private JLabel width=new JLabel("Szererokość");
	/** wybór rozmiaru */
	private JSpinner widthS=new JSpinner(new SpinnerNumberModel(11,3,51,1));
	private JButton gen=new JButton("Generuj");
	/** lista dostępnych algorytmów */
	private JComboBox<String> strategy=new JComboBox<>();
	private JLabel db=new JLabel("Baza: ");
	/** ścieżka do bazy */
	private JTextField dataBase=new JTextField(17);
	private JButton browseDB=new JButton("...");
	/** referencja do aktualnie załadowanej bazy danych, w celu optymalizacji działania */
	private InteliCwDB loadedDB;
	/** klasa nadrzędna */
	private MainFrame mainFrame;
	
	/** 
	 * Konstruktor klasy tworzy grupę kontrolek odpowiedzialnych za działanie generatora
	 * @param main referencja do klasy nadrzędnej
	 */
	Generate(MainFrame main, final Preferences prefs) {
		mainFrame=main;
		//ustawienie stylu
		setBorder(new TitledBorder("Generowanie"));
		setLayout(new FlowLayout());
		//dodanie kontrolek
		add(height); add(heightS); add(width);
		add(widthS); add(gen);
		strategy.addItem("Simple"); strategy.addItem("Advanced"); strategy.addItem("NewAdvanced");
		add(strategy);
		browseDB.setMargin(new Insets(1,1,1,1));
		add(db);
		add(dataBase); add(browseDB);
		
		//kliknięcie generuj
		gen.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					Strategy selectedStrategy=(Strategy)Class.forName(
								"crossword.strategy."+(String)strategy.getSelectedItem()).newInstance(); //utworzenie obiektu strategii
					if(loadedDB==null) //jeśli baza nie została wczytana
						throw new IOException("Nie podano ścieżki do bazy");
					//generowanie bazy danych
					mainFrame.cwBrowser.generate(selectedStrategy, (Integer)heightS.getValue(), (Integer)widthS.getValue(), loadedDB);
					mainFrame.cwPanel.setSolve(false);
					//zapis preferencji
					prefs.putInt("height", (Integer)heightS.getValue());
					prefs.putInt("width", (Integer)widthS.getValue());
					prefs.put("strategy", (String)strategy.getSelectedItem());
				} catch (IOException | ClassNotFoundException | IllegalAccessException | InstantiationException e) {
					JOptionPane.showMessageDialog(mainFrame, e.getMessage(),"Błąd generacji", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				}  catch (Exception e) {
					JOptionPane.showMessageDialog(mainFrame, "Nieznany błąd","Błąd generacji", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				}  
			}
		});
		
		//wybór bazy danych
		browseDB.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ev) {
				try {
					mainFrame.fileWindow.resetChoosableFileFilters();		//usunięcie filtrów wyboru
					mainFrame.fileWindow.setFileSelectionMode(JFileChooser.FILES_ONLY); //tylko pliki (dowolne rozszerzenie)
					int retVal=mainFrame.fileWindow.showOpenDialog(mainFrame); //pokaż okno wyboru
					if(retVal==JFileChooser.APPROVE_OPTION) {
						dataBase.setText(mainFrame.fileWindow.getSelectedFile().getCanonicalPath()); //zmień textArea 
						dataBase.postActionEvent();  //trigger Event
					}
				} catch(IOException e) {
					JOptionPane.showMessageDialog(mainFrame, e.getMessage(),
							"Błąd ładowania bazy danych", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				} catch(Exception e) {
					JOptionPane.showMessageDialog(mainFrame, "Nieznany błąd",
							"Błąd ładowania bazy danych", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				}
			}
		});
		
		//zmiana ścieżki do bazy
		dataBase.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ev) {
				try {
					loadedDB=new InteliCwDB(dataBase.getText());
					prefs.put("cwDB", dataBase.getText()); //zmiana preferencji tylko jeśli poprzednia instrukcja się powiodła
				} catch(IOException e) {  //ładowanie nie powiodło się
					loadedDB=null; 
					dataBase.setText("");
					JOptionPane.showMessageDialog(mainFrame, e.getMessage(),
							"Błąd ładowania bazy danych", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				} catch(Exception e) {
					loadedDB=null; 
					dataBase.setText("");
					JOptionPane.showMessageDialog(mainFrame, "Nieznany błąd",
							"Błąd ładowania bazy danych", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				}
			}
		});
		
		try {
			String db=prefs.get("cwDB", null);
			if(db!=null) {
				dataBase.setText(db);
				dataBase.postActionEvent(); //trigger event
			}
			heightS.setValue(prefs.getInt("height", 11));
			widthS.setValue(prefs.getInt("width", 11));
			strategy.setSelectedItem(prefs.get("strategy", "Simple"));
		} catch(Exception e) {
			JOptionPane.showMessageDialog(mainFrame, "Wczytywanie preferencji nie powiodło się", "Błąd", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}
}
