package GUI;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.prefs.Preferences;

import crossword.logic.Crossword;
import crossword.browser.*;

/**
 * Klasa odpowiedzialna za graficzny interfejs Browsera
 * @author Florek
 */
public class Browser extends JPanel {
	/** ścieżka do katalogu*/
	private JTextField src=new JTextField(20);
	private JButton browse=new JButton("...");
	private JButton save=new JButton("Zapisz");
	private JButton load=new JButton("Wczytaj");
	/** lista krzyżówek do wybory */
	private JComboBox<String> cws=new JComboBox<>();
	/** referencja do nadklasy */
	private MainFrame mainFrame;
	
	/**
	 * Konsrtruktor tworzy grupę kontrolek odpowiedzialnych za działanie Browsera
	 * @param main referencja do nadklasy
	 */ 
	Browser(MainFrame main, final Preferences prefs) {
		mainFrame=main;
		//style
		setBorder(new TitledBorder("Browser"));
		setLayout(new FlowLayout());
		//wstawianie kontrolek
		browse.setMargin(new Insets(1,1,1,1)); 
		cws.setPreferredSize(new Dimension(110,20));
		add(src);  add(browse); add(save); 
		add(load); add(cws);
		
		//wybór katalogu z krzyżówkami
		browse.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ev) {
				try {
					mainFrame.fileWindow.resetChoosableFileFilters();	//wyczyszczenie filtra który mógł być wcześniej ustawiony
					mainFrame.fileWindow.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY); //tylko katalogi
					int retVal=mainFrame.fileWindow.showOpenDialog(mainFrame); //urucho okno otwirania
					if(retVal==JFileChooser.APPROVE_OPTION) {
						src.setText(mainFrame.fileWindow.getSelectedFile().getCanonicalPath()); //wpisz tekst do kontrolki
						src.postActionEvent(); //wywołaj na kontrolce Event wpisania tekstu
					}
				} catch(IOException e) { //wszystko
					JOptionPane.showMessageDialog(mainFrame, e.getMessage(),
							"Błąd ładowania katalogu z krzyżówkami", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				} catch(Exception e) { //wszystko
					JOptionPane.showMessageDialog(mainFrame, "Nieznany błąd",
							"Błąd ładowania katalogu z krzyżówkami", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				}
			}
		});
		
		//ustawianie nowego klatalogu z krzyżówkami
		src.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ev) {
				try {
					mainFrame.cwBrowser.setDir(src.getText());  //próba ustawienia cwBrowsera na nowy katalog
					prefs.put("cwsDir", src.getText());   //zmiana preferencji tylko jeśli poprzednia instrukcja się powiodła
				} catch(IOException e) { //jeśli się nie powiodła
					src.setText(mainFrame.cwBrowser.dirPath()==null ? "" : mainFrame.cwBrowser.dirPath().getAbsolutePath()); //przywrócenie starego
					JOptionPane.showMessageDialog(mainFrame, e.getMessage(), 				//użyłe AbsolutePath, ponieważ wcześniej użyty CanocnialPath powinien ustawić ładną ścieżkę
							"Błąd ładowania katalogu z krzyżówkami", JOptionPane.ERROR_MESSAGE); //infromacja o błędzie
					e.printStackTrace();
				} catch(Exception e) {
					src.setText(mainFrame.cwBrowser.dirPath()==null ? "" : mainFrame.cwBrowser.dirPath().getAbsolutePath()); //przywrócenie starego
					JOptionPane.showMessageDialog(mainFrame, "Nieznany błąd", "Błąd ładowania katalogu z krzyżówkami", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				}
			}
		});
		
		//zapis krzyżówki
		save.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ev) {
				try {
					mainFrame.cwBrowser.save();
					JOptionPane.showMessageDialog(mainFrame, "Zapisana", "Sukces",JOptionPane.INFORMATION_MESSAGE);
				} catch (IOException | BrowserException e) { //obsługa błędów to wypisanie informacji (każdy obiekt Exception ma w msg napisaną przyczynę błędu którą wyświetlam)
					JOptionPane.showMessageDialog(mainFrame, e.getMessage(), "Błąd zapisu", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				} catch(Exception e) {
					JOptionPane.showMessageDialog(mainFrame, "Nieznany błąd", "Błąd zapisu", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				}
			}
		});
		
		//wczytanie kzyżówek
		load.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					mainFrame.cwBrowser.load();
					ArrayList<Crossword> crosswords=mainFrame.cwBrowser.getAvailableCw();
					cws.removeAllItems();	//czyszczenie starej listy
					for(Crossword cw: crosswords)
						cws.addItem(cw.getID()+""); //dodawanie wczytanych
				} catch (IOException | BrowserException e) {	//obsługa błędów to wyświetlenie informacji o błędzie (korzysta z getMessage)
					JOptionPane.showMessageDialog(mainFrame, e.getMessage(), "Błąd wczytywania", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				} catch(Exception e) {
					JOptionPane.showMessageDialog(mainFrame, "Nieznany błąd", "Błąd wczytywania", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				}
			}
		});
		
		//wybór krzyżówki z comboboxa
		cws.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(cws.getItemCount()!=0) {
					mainFrame.cwBrowser.setPresentCw(mainFrame.cwBrowser.getAvailableCw().get(cws.getSelectedIndex())); //uswatwienie wybranej krzyżówki w browserze
					mainFrame.cwPanel.setSolve(false);
				}
			}
		});	
		
		//Ustawienie preferencji
		try {
			String cwDir=prefs.get("cwsDir", null);
			if(cwDir!=null) {
				src.setText(cwDir);
				src.postActionEvent();
			}
		} catch(Exception e) {
			JOptionPane.showMessageDialog(mainFrame, "Wczytywanie preferencji nie powiodło się", "Błąd", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}
}
