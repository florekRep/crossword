package GUI;

import java.util.*;
import java.awt.*;
import java.awt.print.*;

import javax.swing.*;

import com.itextpdf.text.PageSize;

import crossword.logic.*;
import crossword.board.*;

/**
 * Klasa odpowiedzalna za wyświetlanie/drukowanie krzyżówki
 * @author Florek
 *
 */
public class CrosswordPanel extends JPanel {
	/** rozmiar boku kwadratu */
	private static final int side=25;
	/** rozmiar czcionki */
	private static final int clueFontSize=15;
	/** odstępy */
	private static final int emptySpace=2*side;
	/** maks długość tekstu */
	private static final int maxString=575;
	
	/** referencja do klasy nadrzędnej */
	private MainFrame mainFrame;
	/** czy printować litery */
	private boolean solve=false;			
	
	/** haniebna zmienna którą ustawiam w paint a wykorzystuję w print */
	private int cwHeight;
	
	/**
	 * Konstruktor ustawia klasę nadrzędną
	 * @param mainFrame klasa nadrzędna
	 */
	CrosswordPanel(MainFrame mainFrame) {
		this.mainFrame=mainFrame;
	}
	/**
	 * czy printować litery
	 * @param solve true z rozwiązaniem, false bez
	 */
	public void setSolve(boolean solve) {
		this.solve=solve;
		repaint();
	}
	/**
	 * Wyrysowanie krzyżówki na ekranie
	 */
	public void paintComponent(Graphics g) {								
		super.paintComponent(g);	
		Crossword cw=mainFrame.cwBrowser.getPresentCw();		//pobranie referencji do aktualnej krzyżówki
		this.setBackground(Color.WHITE);		//tło białe
		if(cw==null) {
			setPreferredSize(new Dimension(0,0)); //dzięki temu drukowanie będzie niemożliwe
			return;		//brak krzyżówki więc nie ma co rysować, koniec
		}
		Board b=cw.getBoardCopy();  //pobranie kopi plansszy
		
		g.setFont(new Font("Helvetica", Font.PLAIN, clueFontSize)); //ustawienie czcionki
		FontMetrics fm=g.getFontMetrics();							//oraz pobranie jej metryki
		
		//printowanie boardu
		for(int i=0; i<b.getHeight(); i++)
			for(int j=0; j<b.getWidth(); j++) {
				String letter=b.getCell(i, j).getContent();		//pobranie litery z komórki
				if(letter!=null) {		//jeśli komórka nie jest pusta
					g.drawRect(emptySpace+j*side, emptySpace+i*side, side, side);	//rysuj kwadrat
					if(solve) {			//jeśli drukowanie liter włączone
						letter=letter.toUpperCase();   
						g.drawString(letter, emptySpace+j*side+((side-fm.stringWidth(letter))/2), 
								             emptySpace+(i+1)*side-((side-clueFontSize)/2));  //wpisanie litery
					}
				}
			}
		
		//printowanie wskazówek
		g.setFont(g.getFont().deriveFont((float)(clueFontSize-1)));	//pominejszenie czcionki
		fm=g.getFontMetrics();
		
		int horizCounter=1, vertCounter=1, maxX=0;
		cwHeight=side*b.getHeight(); 		//ustawienie haniebnej zmiennej
		int startY=(3*emptySpace+cwHeight);	//odkąd zaczynami wpisywać wskazówki
		
		startY-=(startY%clueFontSize);		//dorównanie w taki sposób aby rozmiar był wielokrotności wysokości czcionki
		startY-=fm.getDescent();			//przydatne podczas dzielenia na strony
		int savedPos=startY;				//zapisanie dla nagłówka
		for(Iterator<CwEntry> it=cw.getROEntryIter(); it.hasNext(); ) {
			CwEntry entry=it.next();
			if(entry.getDir()==Direction.HORIZ) {	//dla każdego poziomego hasła
				String txt=String.format("%2d. %s", horizCounter,entry.getClue()); //przygotuj wskażówkę
				if(fm.stringWidth(txt)>=maxString) {	//sprawdź czy szerkość nie przekracza jednej strony
					int pos=txt.lastIndexOf(' ');		//jeśli tak następuje podział na mniejsze części
					while(fm.stringWidth(txt.substring(0, pos))>=maxString) 
						pos=txt.lastIndexOf(' ',pos-1);
					int startLen=txt.indexOf('.');
					startLen=fm.stringWidth(txt.substring(0,startLen+2));
					g.drawString(txt.substring(0,pos), emptySpace, startY+horizCounter*clueFontSize);
					do {
						pos++;
						startY+=clueFontSize;
						int pos2=txt.length();
						while(fm.stringWidth(txt.substring(pos, pos2))>=maxString-startLen) 
							pos2=txt.lastIndexOf(' ',pos2-1);
						g.drawString(txt.substring(pos, pos2), emptySpace+startLen, startY+horizCounter*clueFontSize);
						pos=pos2;
					} while(pos!=txt.length());
					maxX=maxString;
				} else {			//jeśli nie to narysuj zwyczajnie
					g.drawString(txt,emptySpace,startY+horizCounter*clueFontSize); 
					if(fm.stringWidth(txt)>maxX)	//sprawdź czy nowe hasło wyznacza nową szerokość 
						maxX=fm.stringWidth(txt);
				}
				txt=""+horizCounter;		//dodaj numerek na planszy	
				g.drawString(txt, emptySpace+(entry.getY())*side-fm.stringWidth(txt)-2,
					     emptySpace+(entry.getX()+1)*side-clueFontSize+2);
				horizCounter++;
			}
		}
		if(horizCounter!=1) g.drawString("Poziomo", emptySpace, savedPos);  //jeśli istniały hasła poziome to trzeba dodać nagłówek
		
		startY+=2*clueFontSize+clueFontSize*horizCounter; //wyznaczenie gdzie rysowane będą hasła pionowe
		savedPos=startY;	//zapisanie dla nagłówka
		for(Iterator<CwEntry> it=cw.getROEntryIter(); it.hasNext(); ) {
			CwEntry entry=it.next();
			if(entry.getDir()==Direction.VERT) {
				String txt=String.format("%2d. %s", vertCounter,entry.getClue());
				if(fm.stringWidth(txt)>=maxString) {	//sprawdź czy szerkość nie przekracza jednej strony
					int pos=txt.lastIndexOf(' ');		//jeśli tak następuje podział na mniejsze części
					while(fm.stringWidth(txt.substring(0, pos))>=maxString) 
						pos=txt.lastIndexOf(' ',pos-1);
					int startLen=txt.indexOf('.');
					startLen=fm.stringWidth(txt.substring(0,startLen+2));
					g.drawString(txt.substring(0,pos), emptySpace, startY+vertCounter*clueFontSize);
					do {
						pos++;
						startY+=clueFontSize;
						int pos2=txt.length();
						while(fm.stringWidth(txt.substring(pos, pos2))>=maxString-startLen) 
							pos2=txt.lastIndexOf(' ',pos2-1);
						g.drawString(txt.substring(pos, pos2), emptySpace+startLen, startY+vertCounter*clueFontSize);
						pos=pos2;
					} while(pos!=txt.length());
					maxX=maxString;
				} else {
					g.drawString(txt,emptySpace,startY+vertCounter*clueFontSize);
					if(fm.stringWidth(txt)>maxX)
						maxX=fm.stringWidth(txt);
				}
				txt=""+vertCounter;			//dodaj numer na planszy
				g.drawString(txt, emptySpace+(entry.getY())*side+2,
						     emptySpace+(entry.getX())*side-2);
				vertCounter++;
			}
		}	
		if(vertCounter!=1) 	g.drawString("Pionowo", emptySpace, savedPos); //jeśli istniały...
		
		//ustawienie rozmiarów, konieczne dla JScrollPanelu
		this.setPreferredSize(new Dimension(
				Math.max(emptySpace*2+b.getWidth()*side, maxX+emptySpace*2),
				startY+vertCounter*clueFontSize));
		//odświeżenie w celu aktalizacji JScrolla
		this.revalidate();
	}
	
	/**
	 * Zwraca obiekt drukujący
	 * @return obiekt drukujący
	 */
	public Pageable getPrinter() {
		return new Printer();
	}
	/**
	 * Implementacja klasy służącej do drukowania panelu
	 * @author Florek
	 *
	 */
	class Printer implements Pageable {
		/** współczynnik pomniejszenia */
		private static final double factor=0.8;
		/** rozmiary stron do druku*/
		private int totalPages;
		private int cwPagesWidth;
		private int cwPagesHeight;
		/** format strony */
		private PageFormat pf;
		
		private int NWD(int a, int b) {
	        if (b == 0){ return a; }
	        return NWD(b, (a % b));
	    }
		private int NWW(int a, int b) {
			return a*b/NWD(a,b);
		}
		/** wyrównanie do zadanej liczby */
		private int align(int val, int to) {
			if(val%to!=0)
				val-=(val%to);
			return val;
		}
		/**
		 * konstruktor ustala rozmiar strony na której będzie drukował
		 */
		public Printer() {
			Paper a4=new Paper();
			//standardowa A4 z przesuniętymi marginesami oraz wyrównania do NWW(side,font) i side
			a4.setImageableArea(40, 60, 
					align((int)a4.getImageableWidth()+40,(int)(factor*CrosswordPanel.this.side)),
					align((int)a4.getImageableHeight()+60,(int)(factor*NWW((int)CrosswordPanel.this.side,(int)CrosswordPanel.this.clueFontSize))));
			pf=new PageFormat();
			pf.setPaper(a4);  //ustawienie formatu
			//policzenie rozmiaru stron do druku
			cwPagesWidth=(int)Math.ceil(factor*CrosswordPanel.this.getPreferredSize().width/pf.getImageableWidth());
			cwPagesHeight=(int)Math.ceil(factor*CrosswordPanel.this.cwHeight/pf.getImageableHeight());
			totalPages=(cwPagesWidth-1)*cwPagesHeight;
			totalPages+=(int)Math.ceil(factor*CrosswordPanel.this.getPreferredSize().height/pf.getImageableHeight());
		}
		/**
		 * Zwraca liczbę stron
		 */
		@Override
		public int getNumberOfPages() {	
			return totalPages;
		}
		/**
		 * Zwraca format stron
		 */
		@Override
		public PageFormat getPageFormat(int page) throws IndexOutOfBoundsException {
			return pf;
		}
		/**
		 * Zwraca obiekt Printable który przeprowadza drukowanie
		 */
		@Override
		public Printable getPrintable(int pageIndex) throws IndexOutOfBoundsException {
			return printer;
		}
		/**
		 * Implementacja Printable
		 */
		private Printable printer= new Printable() {
			/**
			 * Funkcja odpowiada za przeprowadzenie drukowania
			 */
			@Override
			public int print(Graphics g, PageFormat pf,
					int pageIndex) throws PrinterException {
				if (pageIndex > totalPages)  //nie ma już nic do drukowania
			        return NO_SUCH_PAGE;
			    Graphics2D g2d = (Graphics2D)g;
			    if(pageIndex<cwPagesWidth*cwPagesHeight) {  //jeśli drukowana jest krzyżówka
			    	g2d.translate(pf.getImageableX()-(pageIndex%cwPagesWidth)*(pf.getImageableWidth()), 
			    			pf.getImageableY()-(pageIndex/cwPagesWidth)*(pf.getImageableHeight()));  //translacja względem x,y
			    } else {  //jeśli drukowane są hasła
			    	g2d.translate(pf.getImageableX(), 
			    			pf.getImageableY()-(pageIndex-cwPagesWidth*cwPagesHeight+cwPagesHeight)*(pf.getImageableHeight()));
			    				//translacja względem y
			    }
		   	    g2d.scale(factor, factor);	//skalowanie
			    CrosswordPanel.this.printAll(g); //drukowanie
			    return PAGE_EXISTS;
			}
		};
	};
	
}
