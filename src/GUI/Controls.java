package GUI;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.print.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.prefs.Preferences;

import com.itextpdf.awt.*;
import com.itextpdf.text.*;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.*;

/**
 * Klasa odpowiedzialna za graficzny interfejs kontroli
 * @author Florek
 *
 */
public class Controls extends JPanel {
	private JButton print=new JButton("Drukuj");
	private JButton PDF=new JButton("PDF");
	private JButton solve=new JButton("Rozwiąż");
	/** klasa nadrzędna */
	private MainFrame mainFrame;
	
	/**
	 * Konsrtruktor tworzy grupę kontrolek odpowiedzialnych za działanie Kotroli
	 * @param main referencja do nadklasy
	 */ 
	Controls(MainFrame main) {
		mainFrame=main;
		//style
		setBorder(new TitledBorder("Kontrola"));
		setLayout(new FlowLayout());
		//dodawanie kontrolek
		add(solve); add(PDF); add(print);
		
		//Rozwiąż
		solve.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				mainFrame.cwPanel.setSolve(true);
			}
		});
		
		//Drukowanie
		print.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					Pageable printer=mainFrame.cwPanel.getPrinter();
					if(printer.getNumberOfPages()==0) 
						throw new PrinterException("Brak stron do drukowania");
					PrinterJob job = PrinterJob.getPrinterJob();		//zastartowanie zadania drukowania
					job.setPageable(printer);		//ustawienie obiektu do drukowania
					job.setJobName("Crossword");
			        if (job.printDialog()) 	//okono wyboru opcji
			        	job.print(); //drukowanie
				} catch (PrinterException e) {
	            	JOptionPane.showMessageDialog(mainFrame,e.getMessage(),"Błąd drukowania",JOptionPane.ERROR_MESSAGE);
	            	e.printStackTrace();
	            } catch(Exception e) {
	            	JOptionPane.showMessageDialog(mainFrame,"Nieznany błąd","Błąd drukowania",JOptionPane.ERROR_MESSAGE);
	            	e.printStackTrace();
	            }
			}
		});
		
		//Drukowanie do PDF, wykorzystuje pakiet IText
		PDF.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					Pageable printer=mainFrame.cwPanel.getPrinter();	//pobranie obiektu służącego do drukowania
					if(printer.getNumberOfPages()==0) 
						throw new PrinterException("Brak stron do drukowania");
					
					mainFrame.fileWindow.setFileSelectionMode(JFileChooser.FILES_ONLY);	
					mainFrame.fileWindow.setFileFilter(new FileNameExtensionFilter(".*pdf", "pdf")); //ustwienie filtra na PDF
					int retVal=mainFrame.fileWindow.showSaveDialog(mainFrame); //opcja wyboru do zapisu
					if(retVal!=JFileChooser.APPROVE_OPTION) //wybrano anuluj
						return; 
					File file=mainFrame.fileWindow.getSelectedFile(); //wybrany plik
					
					String fname=file.getName();	//sprawdzenie czy kończy się na .pdf jeśli nie to dodanie 
					int pos=fname.lastIndexOf('.');
					if(pos==-1 || !fname.substring(pos).equals(".pdf"))
						file=new File(file.getCanonicalPath()+".pdf");
						
					FileOutputStream fos = new FileOutputStream(file);
					try {
						PageFormat pf=printer.getPageFormat(0);				//pobranie formatu strony
						Rectangle rect=new Rectangle(0f, 0f, (float)pf.getWidth(), (float)pf.getHeight());
						Document document=new Document(rect);			//tworzenie dokumentu
					
						FontMapper helvetica = new FontMapper() { //utworzenie czcionki (trudność z polskimi znakami
							@Override
							public BaseFont awtToPdf(java.awt.Font font) {
				                try {
				                    return BaseFont.createFont(BaseFont.HELVETICA,BaseFont.CP1250, BaseFont.EMBEDDED);
				                } catch (Exception e) {
				                    e.printStackTrace();
				                }
				                return null;
				            }
				            public java.awt.Font pdfToAwt(BaseFont font, int size) {
				                return null;
				            }
				        };
		
						PdfWriter writer=PdfWriter.getInstance(document, fos);  //strumień zapisujący do PDF
						writer.setCloseStream(true);		//autozamykanie po zakończeniu
						document.open();					//otwarcie dokumentu
						PdfContentByte contentByte=writer.getDirectContent();  //pobranie Kontentu do zapisu
						for(int i=0; i<printer.getNumberOfPages(); i++) {
							document.newPage();		//nowa strona
							Graphics2D g2d=new PdfGraphics2D(contentByte, (float)pf.getWidth(), (float)pf.getHeight(), helvetica);
							//obcięcie obszaru aby ładnie wpasował się na stronę
							g2d.clipRect((int)pf.getImageableX(), (int)pf.getImageableY(), (int)pf.getImageableWidth(), (int)pf.getImageableHeight());
							Printable printable=printer.getPrintable(i);
							try {
								//drukowanie do obiektu g2d
								printable.print(g2d, pf, i);								
							} finally {
								g2d.dispose();
							}
						}
						document.close(); //zamykanie dokumentu
						JOptionPane.showMessageDialog(mainFrame,"Zapis udany","Sukces",JOptionPane.INFORMATION_MESSAGE);
					} finally {
						fos.close(); //zamknięcie strumienia
					}
				} catch(IOException | PrinterException | DocumentException e) {
					JOptionPane.showMessageDialog(mainFrame,e.getMessage(), "Błąd podczas zapisu do PDF",JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				} catch(Exception e) {
	            	JOptionPane.showMessageDialog(mainFrame,"Nieznany błąd","Błąd drukowania",JOptionPane.ERROR_MESSAGE);
	            	e.printStackTrace();
	            }
			}
		});
	}
}