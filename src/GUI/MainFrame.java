package GUI;

import javax.swing.*;
import crossword.dictionary.*;
import crossword.logic.*;
import crossword.strategy.*;
import crossword.browser.*;

import java.awt.*;
import java.util.prefs.Preferences;

/**
 * Główna klasa grafiki
 * @author Florek
 *
 */
public class MainFrame extends JFrame {
	//istotnym jest fakt że są to klasy o dostepie pakietowym
	JPanel settings=new JPanel();
	CrosswordPanel cwPanel=new CrosswordPanel(this);
	JFileChooser fileWindow=new JFileChooser();
	CwBrowser cwBrowser=new CwBrowser();
	JScrollPane jsp=new JScrollPane(cwPanel);
	
	/**
	 * konstruktor ustawia kontrolki na JFramie
	 */
	public MainFrame() {
		//styl
		settings.setLayout(new WrapLayout());
		//dodawanie koontrolek
		Preferences prefs=Preferences.userNodeForPackage(MainFrame.class);
		settings.add(new Generate(this,prefs));
		settings.add(new Browser(this,prefs));
		settings.add(new Controls(this));
		add(settings, BorderLayout.NORTH);
		add(jsp, BorderLayout.CENTER);
		//przewijanie myszą
		jsp.getVerticalScrollBar().setUnitIncrement(16); 
		jsp.getHorizontalScrollBar().setUnitIncrement(16);
	}
	/**
	 * funkcja odpowiedzialna za wyświetlenie okna
	 * @param f JFrame
	 * @param width szerokość
	 * @param height wysokość
	 */
	public static void run(final JFrame f, final int width, final int height) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				f.setTitle("CrossWord");
				f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				f.setSize(width, height);
				f.setVisible(true);
			}
		});
	}
	/**
	 * Punkt wejścia programu
	 * @param args żadne nie oczekiwane
	 */
	public static void main(String[] args) throws Exception{
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		run(new MainFrame(), 750, 700);
	}
}
